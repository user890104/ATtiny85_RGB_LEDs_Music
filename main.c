#include <stdlib.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>

// Red, Green, Blue
#define NUM_CHANS 3

// we use ADC0, ADC1 and ADC3, so...
#define ADC_CHAN(x) ((x) == 2 ? 3 : (x))

// skip 0, causes the pwm output to invert
#define FIX_VAL(x) ((x) == 0 ? 1 : (x))

// save this one and reuse it when switching ADC chans
#define ADMUX_VAL (_BV(REFS1) | _BV(ADLAR))

register uint8_t chan asm("r2");

void main() {
    chan = 0;

    /*
     * Input:
     * PB5/ADC0: Red/Low
     * PB2/ADC1: Green/Med
     * PB3/ADC3: Blue/High
     * Outputs:
     * PB4/OC1B: Red/Low
     * PB0/OC0A: Green/Med
     * PB1/OC0B: Blue/High
     */

    DDRB = _BV(DDB4) | _BV(DDB1) | _BV(DDB0);

    /*
     * Control Register A for Timer/Counter-0 (Timer/Counter-0 is configured using two registers: A and B)
     * TCCR0A is 8 bits: [COM0A1:COM0A0:COM0B1:COM0B0:unused:unused:WGM01:WGM00]
     * Set bit COM0A1, which (in Fast PWM mode) clears OC0A on compare-match, and sets OC0A at BOTTOM
     * Set bit COM0B1, which (in Fast PWM mode) clears OC0B on compare-match, and sets OC0B at BOTTOM
     * Set bits WGM00 and WGM01, which enables Fast PWM mode
     */

    TCCR0A = _BV(COM0A1) | _BV(COM0B1) | _BV(WGM00) | _BV(WGM01);

    /*
     * Control Register B for Timer/Counter-0 (Timer/Counter-0 is configured using two registers: A and B)
     * TCCR0B is 8 bits: [FOC0A:FOC0B:unused:unused:WGM02:CS02:CS01:CS00]
     * Set bit CS00 (leaving CS01 and CS02 clear), which tells Timer/Counter-0 to not use a prescalar
     */

    TCCR0B = _BV(CS00);

    /*
     * Control Register for Timer/Counter-1 (Timer/Counter-1 is configured with just one register: this one)
     * TCCR1 is 8 bits: [CTC1:PWM1A:COM1A1:COM1A0:CS13:CS12:CS11:CS10]
     * Set bit CS10 which tells Timer/Counter-1  to not use a prescalar
     */

    TCCR1 = _BV(CS10);

    /*
     * General Control Register for Timer/Counter-1 (this is for Timer/Counter-1 and is a poorly named register)
     * GTCCR is 8 bits: [TSM:PWM1B:COM1B1:COM1B0:FOC1B:FOC1A:PSR1:PSR0]
     * Set bit PWM1B which enables the use of OC1B (since we disabled using OC1A in TCCR1)
     * Set bit COM1B1 and leaves COM1B0 clear, which (when in PWM mode) clears OC1B on compare-match, and sets at BOTTOM
     */

    GTCCR = _BV(PWM1B) | _BV(COM1B1);

    /*
     * ADC: 10-bit result(9 + sign), Vref = 1.1V, Left Adjust Result,
     * Input on ADC0 (PB5) (MUX = 0000)
     */

    ADMUX = ADMUX_VAL;

    /*
     * ADC: Bipolar Input Mode
     */

    ADCSRB = _BV(BIN);

    /*
     * ADC: Enable ADC, Enable interrupt, CLK/128
     */

    ADCSRA = _BV(ADEN) | _BV(ADIE) | _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);

    /*
     * Enable global interrupt flag
     */

    sei();

    /*
     * loop forever
     */

    while (1) {
        // in addition to going to sleep,
        // sleep mode starts an ADC converion
        // if ADC is enabled
        sleep_mode();
    }
}

ISR(ADC_vect) {
    // find amplitude and fit in 8 bits
    uint8_t amplitude = FIX_VAL(abs(ADC) >> 7);

    // copy result to output
    if (chan == 0) {
        OCR1B = amplitude;
    }

    if (chan == 1) {
        OCR0A = amplitude;
    }

    if (chan == 2) {
        OCR0B = amplitude;
    }

    // find next chan
    chan++;

    if (chan >= NUM_CHANS) {
        chan = 0;
    }

    // set adc chan for next conversion
    ADMUX = ADMUX_VAL | ADC_CHAN(chan);

    // leaving the ISR jumps into the while() loop in main()
    // and triggers the next conversion
}
